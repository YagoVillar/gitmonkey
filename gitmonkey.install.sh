#/bin/bash
echo "Your actual path is: "
pwd
echo ""
read -p "Type an absolute path to install GitMonkey 1.0: 
" installdoc
cp data/mount.sh $installdoc

if [[ $? -gt 0 ]]; then
    echo "##################################"
    echo "I can't find this path: $installdoc"
    echo "Please try an other time"
    echo "##################################"
    read -p "$*"
    { exit 1; }
fi

cp data/pmount.sh $installdoc
cp data/.monkey_help.txt $installdoc
cp data/.monkey_uninstall.sh $installdoc
touch $installdoc/.monkey_aliases
touch $installdoc/.monkey_commands
touch $installdoc/.monkey_log
touch $installdoc/.monkey_list

#New modules to separate monkey alias of bashrc
echo "if [ -f $installdoc/.monkey_aliases ]; then
  . $installdoc/.monkey_aliases
fi #monkey" >> ~/.bashrc

echo "if [ -f $installdoc/.monkey_commands ]; then
  . $installdoc/.monkey_commands
fi #monkey" >> ~/.bashrc

#Alias declaration on main GitMonkey archives (Gitmonkey Commands)
echo alias gitmonkey-help='"'cat $installdoc/.monkey_help.txt'"' >> $installdoc/.monkey_commands

echo alias gitmonkey-edit='"'nano $installdoc/.monkey_aliases'"' >> $installdoc/.monkey_commands

echo alias gitmonkey-list='"'cat $installdoc/.monkey_list'"' >> $installdoc/.monkey_commands

echo alias gitmonkey-mount='"'sudo . $installdoc/mount.sh'"' >> $installdoc/.monkey_commands

echo alias gitmonkey-log='"'tail -n30 $installdoc/.monkey_log'"' >> $installdoc/.monkey_commands

echo alias gitmonkey-full-log='"'cat $installdoc/.monkey_log'"' >> $installdoc/.monkey_commands

echo alias gitmonkey-uninstall='"'sudo . $installdoc/.monkey_uninstall.sh'"' >> $installdoc/.monkey_commands

echo alias gitmonkey-commit-mount='"'. $installdoc/pmount.sh'"' >> $installdoc/.monkey_commands


#Copying de installation directory route variable to de main mount script.
echo "installdoc=$installdoc" >> $installdoc/mount.sh
echo 'Installation finished. For help type "gitmonkey-help" in command line.'

#Drop uninstall information on .monkey_uninstall.sh file


echo "installdoc=$installdoc" >> $installdoc/.monkey_uninstall.sh
chmod 777 -R $installdoc/
. ~/.bashrc
