#/bin/bash
echo "Your actual path is: "
pwd
echo ""
read -p "Enter the absolute path to the GIT directory to automate:
" maindoc
read -p "What is the nickname for your project?: " nicdoc
began=$(pwd)
cd $maindoc
touch share.sh
echo '
#/bin/bash' >> share.sh
echo "nicdoc=$nicdoc" >> share.sh
echo "maindoc=$maindoc" >> share.sh
echo "installdoc=$installdoc" >> $maindoc/share.sh

echo '
begin=$(pwd)
 cd $maindoc
echo ""                    
echo "#############################################################"
echo "         R U N N I N G   F I L E   U P L O A D"
echo "                         $nicdoc.git                  "
echo "#############################################################"
echo ""
git add * 
echo "####################"  
echo "CONFIRM WHO YOU ARE: "
echo "####################" 
git commit -m "Shared with GitMonkey" 
git push
echo ""
echo "###########################################################"
echo "       U P L O A D                C O M P L E T E D       "
echo "###########################################################"
date >> $installdoc/.monkey_log
echo "$nicdoc was shared" >> $installdoc/.monkey_log
echo " " >> $installdoc/.monkey_log
cd $begin
' >> share.sh
echo 
echo alias up$nicdoc='"'. $maindoc/share.sh'"' >> $installdoc/.monkey_aliases
echo "Alias: $nicdoc" >> $installdoc/.monkey_list
echo "Located on: $maindoc" >> $installdoc/.monkey_list
echo " " >> $installdoc/.monkey_list
cd $began
echo "#######################################"
echo "Successful!! Use up$nicdoc to share this proyect"
echo "#######################################"

#This will register de route of "share.sh" from all asociated proyects for a future uninstall.
echo "rm -rf $maindoc/share.sh" >> $installdoc/.monkey_uninstall.sh 

. ~/.bashrc

